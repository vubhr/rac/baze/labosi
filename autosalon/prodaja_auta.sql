
/*
Autosalon iz Bjelovara vodi evidenciju prodaje automobila:
MARKA  - naziv marke automobila
MODEL  - naziv modela automobila
GODINA - godina prozvodnje
SASIJA - broj šasije automobila
IME - Ime kupca
PREZIME - kupca
DATUM - datum Prodaja automobila
CIJENA - cijena automobila


Na temelju pruženih informacija i podataka u tablici datapool, vaš zadatak je:

Normalizacija podataka:
Analizirajte podatke i identificirajte potencijalne probleme s redundancijom ili nekonzistentnostima.
Predložite strukturu normaliziranih tablica koje će rješavati identificirane probleme.

Definiranje relacija:
Na temelju vaših normaliziranih tablica, definirajte primarne i strane ključeve koji će pomoći u povezivanju tablica.
Osigurajte da su sve relacije između tablica ispravno postavljene. 

Može li se negdje voditi evidencija dodatne opreme i da se na temelju toga vodi ukupna cijena?
Da li bi u vaš postojeći model mogli dodati i pravne osobe koje kupuju više automobila po jeftinijim cijenama?
Trebali bi i evidenciju uplata rata ukoliko je automobil kupljen na naš kredit?

*/
DROP TABLE datapool;

-- Kreiranje tablice datapool
CREATE TABLE datapool (
    Marka VARCHAR2(100) NOT NULL,
    Model VARCHAR2(100) NOT NULL,
    Godina NUMBER(4) NOT NULL,
    sasija VARCHAR2(50) UNIQUE NOT NULL,
    ime VARCHAR2(100) NOT NULL,
    Prezime VARCHAR2(100) NOT NULL,
    DATUM DATE NOT NULL,
    Cijena NUMBER(10,2) NOT NULL
);

-- Insert podaci u datapool
INSERT INTO datapool VALUES('Volkswagen', 'Golf', 2019, 'WVWZZZ1JZ3W386752', 'Ivan', 'Ivić', TO_DATE('2022-05-01','YYYY-MM-DD'), 120000.75);
INSERT INTO datapool VALUES('Volkswagen', 'Golf', 2019, 'WVWZZZ1JZ3W386753', 'Ana', 'Anić', TO_DATE('2022-06-10','YYYY-MM-DD'), 120000.75);
INSERT INTO datapool VALUES('Volkswagen', 'Passat', 2018, 'WVWZZZ1JZ3W486754', 'Marko', 'Marić', TO_DATE('2022-04-15','YYYY-MM-DD'), 150000.50);
INSERT INTO datapool VALUES('Volkswagen', 'Golf', 2019, 'WVWZZZ1JZ3W386755', 'Ivan', 'Ivić', TO_DATE('2022-05-20','YYYY-MM-DD'), 120000.75);
INSERT INTO datapool VALUES('Audi', 'A3', 2020, 'WAUZZZ8P6AA058268', 'Petra', 'Petrović', TO_DATE('2022-05-25','YYYY-MM-DD'), 180000.25);
INSERT INTO datapool VALUES('Audi', 'A4', 2021, 'WAUZZZ8P6AA058269', 'Stipe', 'Stipić', TO_DATE('2022-06-05','YYYY-MM-DD'), 200000.00);
INSERT INTO datapool VALUES('Audi', 'A3', 2020, 'WAUZZZ8P6AA058270', 'Petra', 'Petrović', TO_DATE('2022-06-11','YYYY-MM-DD'), 180000.25);
INSERT INTO datapool VALUES('Audi', 'A4', 2021, 'WAUZZZ8P6AA058271', 'Luka', 'Lukić', TO_DATE('2022-06-19','YYYY-MM-DD'), 200000.00);
INSERT INTO datapool VALUES('BMW', '320', 2017, 'WBA8E9C54HK898412', 'Eva', 'Evans', TO_DATE('2022-04-25','YYYY-MM-DD'), 210000.25);
INSERT INTO datapool VALUES('BMW', '320', 2017, 'WBA8E9C54HK898413', 'Luka', 'Lukić', TO_DATE('2022-04-29','YYYY-MM-DD'), 210000.25);
INSERT INTO datapool VALUES('BMW', '330', 2018, 'WBA8E9C54HK898414', 'Jana', 'Janović', TO_DATE('2022-05-30','YYYY-MM-DD'), 220000.00);
INSERT INTO datapool VALUES('Volkswagen', 'Golf', 2019, 'WVWZZZ1JZ3W386756', 'Marko', 'Marić', TO_DATE('2022-06-01','YYYY-MM-DD'), 120000.75);
INSERT INTO datapool VALUES('Mercedes', 'C200', 2019, 'WDDGF4HB9EA956739', 'Luka', 'Lukić', TO_DATE('2022-06-20','YYYY-MM-DD'), 240000.00);
INSERT INTO datapool VALUES('Mercedes', 'C200', 2019, 'WDDGF4HB9EA956740', 'Stipe', 'Stipić', TO_DATE('2022-06-21','YYYY-MM-DD'), 240000.00);
INSERT INTO datapool VALUES('Opel', 'Astra', 2016, 'W0LPD8ED2G1136164', 'Jana', 'Janović', TO_DATE('2022-05-10','YYYY-MM-DD'), 95000.95);
INSERT INTO datapool VALUES('Opel', 'Insignia', 2017, 'W0LPD8ED2G1136165', 'Eva', 'Evans', TO_DATE('2022-05-15','YYYY-MM-DD'), 120000.50);
INSERT INTO datapool VALUES('Opel', 'Astra', 2016, 'W0LPD8ED2G1136166', 'Jana', 'Janović', TO_DATE('2022-05-11','YYYY-MM-DD'), 95000.95);
INSERT INTO datapool VALUES('Peugeot', '308', 2018, 'VF3LCBHZCJS508724', 'Ana', 'Anić', TO_DATE('2022-06-09','YYYY-MM-DD'), 115000.60);
INSERT INTO datapool VALUES('Peugeot', '508', 2019, 'VF3LCBHZCJS508725', 'Ivan', 'Ivić', TO_DATE('2022-06-02','YYYY-MM-DD'), 170000.50);
INSERT INTO datapool VALUES('Renault', 'Megane', 2020, 'VF1RFA00257119469', 'Marko', 'Marić', TO_DATE('2022-05-05','YYYY-MM-DD'), 130000.75);
commit;

--RJEŠENJE

drop table prodaje;
drop table auti;
drop table modeli;
drop table kupci;



--tablica modeli
select * from datapool;
select distinct marka, model from datapool;
select distinct marka, model, length(marka) from datapool;

CREATE TABLE modeli (
    id NUMBER(9) GENERATED ALWAYS AS IDENTITY,
    marka VARCHAR2(20) NOT NULL,
    model VARCHAR2(20) NOT NULL,
    CONSTRAINT pk_modeli PRIMARY KEY (id)
);

insert into modeli (marka, model) 
select 
   distinct 
   marka, 
   model 
from 
   datapool;

select * from modeli;

--tablica auti
select * from datapool;
select distinct sasija, godina, cijena from datapool;
select distinct sasija, godina, cijena, length(sasija), length(cijena) from datapool;

CREATE TABLE auti (
    ID NUMBER(9) GENERATED ALWAYS AS IDENTITY,
    IDMODELA NUMBER(9) NOT NULL,
    sasija VARCHAR2(17) NOT NULL,
    GODINA NUMBER(4) NOT NULL, 
    CIJENA DECIMAL(12,2) NOT NULL,
    CONSTRAINT pk_auti PRIMARY KEY (id),
    CONSTRAINT fk_modeli FOREIGN KEY (idmodela) REFERENCES modeli(id)  
);


insert into auti (IDMODELA, sasija, GODINA,CIJENA)
select 
  (select m.id from modeli m where d.model = m.model and d.marka = m.marka), 
   d .sasija, 
   d.godina, 
   d.cijena 
from 
   datapool d;

select * from auti;
--test, ova dva auta su ista marka i model
select * from datapool where sasija in ('WVWZZZ1JZ3W386752', 'WVWZZZ1JZ3W386753');

--tablica kupci
select * from datapool;
select distinct ime, prezime from datapool;
--provjera da li korisnik može kupiti više automobila
select ime, prezime, count(1) from datapool group by ime, prezime;
select ime, prezime, length(ime), length(prezime)  from datapool group by ime, prezime;

CREATE TABLE kupci (
    ID NUMBER(9) GENERATED ALWAYS AS IDENTITY,
    IME VARCHAR2(20) NOT NULL,
    PREZIME VARCHAR2(20) NOT NULL,
    CONSTRAINT pk_kupci PRIMARY KEY (id)
);

insert into kupci (ime, prezime)
select distinct ime, prezime from datapool;

select * from kupci;

--tablica prodaje
select * from datapool;

CREATE TABLE PRODAJE (
    ID NUMBER(9) GENERATED ALWAYS AS IDENTITY,
    IDAUTA NUMBER(9) NOT NULL,
    IDKUPCA NUMBER(9) NOT NULL,
    DATUM DATE NOT NULL,
    CONSTRAINT pk_prodaje PRIMARY KEY (id),
    CONSTRAINT fk_auti FOREIGN KEY (idauta) REFERENCES auti(id),
    CONSTRAINT fk_kupci FOREIGN KEY (idkupca) REFERENCES kupci(id)  
);

insert into prodaje (IDauta, IDkupca, DATUM) 
select 
   (select id from auti a where a.sasija = d.sasija), 
   (select id kupca from kupci k where k.ime = d.ime and k.prezime = d.prezime), 
   DATUM 
from 
   datapool d;
   
  
--datapool povezano putem novog modela
select 
  *
from
  modeli m,
  auti a,
  prodaje p,
  kupci k
where
  a.idmodela = m.id and
  p.idauta = a.id and
  p.idkupca = k.id;
  
--koji modeli godišta su najprodavaniji  
SELECT 
  m.marka, 
  m.model, 
  a.godina, 
  COUNT(1) as prodano,
  SUM(a.cijena) as ukupno
FROM 
  modeli m, 
  auti a, 
  prodaje p
WHERE 
  a.idmodela = m.id AND
  p.idauta = a.id
GROUP BY 
  m.marka, 
  m.model, 
  a.godina
ORDER BY PRODANO DESC;

INSERT INTO auti (IDMODELA, sasija, GODINA, CIJENA) VALUES (1, 'WVWZZZ1JZ3W386756', 2019, 120000.75);
INSERT INTO auti (IDMODELA, sasija, GODINA, CIJENA) VALUES (2, 'WVWZZZ1JZ3W386757', 2020, 130000.50);
INSERT INTO auti (IDMODELA, sasija, GODINA, CIJENA) VALUES (3, 'WVWZZZ1JZ3W386758', 2018, 140000.00);
INSERT INTO auti (IDMODELA, sasija, GODINA, CIJENA) VALUES (4, 'WVWZZZ1JZ3W386759', 2021, 150000.25);
INSERT INTO auti (IDMODELA, sasija, GODINA, CIJENA) VALUES (5, 'WVWZZZ1JZ3W386760', 2017, 110000.00);

--popis auta
select 
   m.marka, 
   m.model, 
   a.* 
from 
   auti a, 
   modeli m 
where 
   a.idmodela = m.id;

--slobodni auti
SELECT 
   a.ID,
   m.MARKA,
   m.MODEL,
   a.SASIJA,
   a.GODINA,
   a.CIJENA
FROM 
   auti a
LEFT JOIN 
   prodaje p
ON 
   a.ID = p.IDAUTA
LEFT JOIN 
   modeli m
ON
   m.id = a.idmodela
WHERE 
   p.IDAUTA IS NULL;


  